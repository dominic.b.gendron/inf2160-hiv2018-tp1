# Travail pratique 1 - Énumération de sous-arbres induits en Haskell

## Description

Ce programme permet de générer des graphes, puis de les utiliser dans une 
configuration, ce qui permet d'extraire et d'inclure des Vertex aux choix. 
Il est aussi possible d'utiliser des ISTree qui permet de générer toutes les 
combinaisons possibles d'inclusion et d'exclusion pour un graphe, ce qui permet
de sortir tous les sous-arbres induits dans un graphe.

## Auteur

Dominic Brodeur-Gendron (BROD17129101)

## Fonctionnement

Le programme possède 3 dépendances optionnelles, `Doctest`, `Haddock` et `Graphviz`.
Voir section dépendance.

En tout temps il est possible d'ouvrir le projet à l'aide de `ghci` et 
d'éxécuter les commandes manuellement.

Autrement, la commande `make` permet de compiler le projet.

Pour lancer les tests, il suffit d'écrire `make test` et pour générer la documentation
il suffit d'écrire `make doc`. 

5 types de graphes sont accessibles; `completeGraph n`, `cycleGraph n`, `wheelGraph`,
`completeBipartiteGraph n m` et `petersenGraph`.

## Contenu du projet

1. `Graph.hs`: Un module qui permet de représenter un graphe simple (non
   orienté);
2. `Configuration.hs`: Un module qui permet de représenter un sous-arbre
   induit, en étiquetant les sommets selon qu'ils sont *inclus*, *exclus* ou
   *libres*.
3. `ISTree.hs`: Un module qui permet de représenter sous forme d'arbre binaire
   et de façon unique tous les sous-arbres induits d'un graphe donné.
4. `Main.hs`: Le module permettant d'exécuter le programme et de générer une image.

Voici un exemple
![](treeBi.png)

Un autre exemple est dans les fichiers, l'image est simplement trop grosse grâce
à la nature exponentielle des sous-arbres induits.

## Dépendances

Si vous avez installé le logiciel
[Doctest](https://github.com/sol/doctest#readme), il est possible de lancer une
suite de tests de façon automatique à l'aide de la commande
```sh
make test
```

Si vous avez installé le logiciel [Haddock](https://www.haskell.org/haddock/),
vous pouvez en tout temps générer la documentation du projet. Pour cela, il
suffit d'entrer la commande
```sh
make doc
```
Ensuite, il suffit d'ouvrir le fichier `doc/index.html`.


Si vous avez installé le logiciel [Graphviz](https://www.graphviz.org/),
vous pouvez en tout temps générer un `Graph`, une `Configuration` ou un `ISTree`.
Les commandes se trouvent dans les fichiers du même nom.
Pour générer le tout, ouvrez `Main.hs` à l'aide de `ghci` puis entrez `main`.


## Statut

La fonction `namedConfigurations` n'est pas completé.
Le reste des tests marchent.
