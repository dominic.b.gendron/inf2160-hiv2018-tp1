{-|
Module      : Configuration
Description : A module for representing an induced subtree of a graph
Copyright   : (c) Alexandre Blondin Massé
License     : GPL-3
Maintainer  : blondin_masse.alexandre@uqam.ca
Stability   : experimental

This module provides a data structure called 'Configuration', whose purpose is
to represent an induced subtree. Each vertex of a configuration is either
included, excluded or free.
 -}
module Configuration (
    -- * Type and constructor
    Configuration, initConfiguration,
    -- * Configuration update
    includeVertex, excludeVertex, availableVertex,
    -- * Queries
    subtree, subtreeSize, numLeaves, extendable,
    -- * Graphviz output
    toGraphvizString,
    ) where

import qualified Data.Map.Strict as Map
import Data.List (intercalate, sort)
import Data.Maybe (fromJust, fromMaybe, isJust)
import Graph hiding (toGraphvizString)
import qualified Graph (toGraphvizString)

------------------------
-- Status of vertices --
------------------------

-- | The possible status of vertices in a configuration:
--
-- * `Include`: the vertex is included in the subtree;
-- * `Excluded`: the vertex is excluded from the subtree;
-- * `Free`: the vertex is avaible for inclusion or exclusion.
data Status = Included | Excluded | Free
    deriving (Eq, Show)

-------------------
-- Configuration --
-------------------


-- | A configuration for enumerating induced subtrees.
data Configuration v = Configuration {
    configurationMap   :: Map.Map v Status,
    configurationGraph :: Graph v
}

instance (Show v, Ord v) => Show (Configuration v) where
    show c@(Configuration vertexToStatus _) = "Configuration("
        ++ "Included={" ++ (showByStatus Included) ++ "}, "
        ++ "Excluded={" ++ (showByStatus Excluded) ++ "}, "
        ++ "Free={" ++ (showByStatus Free) ++ "})"
        where showByStatus s = intercalate "," $ map show $ verticesByStatus s c

-----------------
-- Constructor --
-----------------

-- | Returns a configuration for the given graph
--
-- At first, all vertices have status @Free@.
--
-- >>> initConfiguration $ wheelGraph 4
-- Configuration(Included={}, Excluded={}, Free={0,1,2,3,4})
initConfiguration :: Ord v => Graph v -> Configuration v
initConfiguration g =
       Configuration {
                configurationMap = Map.fromList [(x,Free) | x <- vertices g],
                configurationGraph = g
       }

-- initConfiguration (Graph {configurationMap = m,configurationGraph = g}) =

-- | Updates the status of the given vertex in a configuration
--
-- >>> updateStatus 1 Included (initConfiguration $ wheelGraph 4)
-- Configuration(Included={1}, Excluded={}, Free={0,2,3,4})
updateStatus :: Ord v => v -> Status -> Configuration v -> Configuration v
updateStatus v s c@(Configuration m g) = 
        Configuration {
                configurationMap = Map.insert v s m,
                configurationGraph = g
        }

-- | Includes the given vertex to the configuration
--
-- If the vertex is not available, an error is triggered.
--
-- >>> includeVertex 2 $ initConfiguration $ wheelGraph 4
-- Configuration(Included={2}, Excluded={}, Free={0,1,3,4})
--
-- In some cases, the inclusion of the vertex might trigger the exclusion of
-- other vertices. More precisely, some vertices could not be included anymore
-- without creating a cycle, so that they can safely be excluded.
--
-- >>> let configuration = initConfiguration (wheelGraph 4)
-- >>> includeVertex 2 $ includeVertex 1 $ initConfiguration $ wheelGraph 4
-- Configuration(Included={1,2}, Excluded={0}, Free={3,4})
includeVertex :: Ord v => v -> Configuration v -> Configuration v
includeVertex v c@(Configuration m g) = 
        if inConfig && isFree
            then tryExcludeVertex (verticesByStatus Free c) $ updateStatus v Included c
        else error "The vertex must be in the graph and Free"
        where inConfig = isJust $ Map.lookup v m
              isFree = Free == (fromJust $ Map.lookup v m)

-- | Will look at every Free vertex and will try to exclude then with the use 
-- of willIncludeMakeCycle
tryExcludeVertex :: Ord v => [v] -> Configuration v -> Configuration v
tryExcludeVertex [] c = c
tryExcludeVertex (x:xs) c = 
        if willIcludeMakeCycle c x
            then tryExcludeVertex xs $ excludeVertex x c
        else tryExcludeVertex xs c


-- | Returns true if the given vertex will make a cycle if Included
willIcludeMakeCycle :: Ord v => Configuration v -> v -> Bool
willIcludeMakeCycle c v = 
        if status c v == Free && numStatusNeighbors Included c v > 1
            then True
        else
            False
        


-- | Excludes the given vertex from the configuration
--
-- If the vertex is not available, an error is triggered.
--
-- >>> excludeVertex 2 $ initConfiguration $ wheelGraph 4
-- Configuration(Included={}, Excluded={2}, Free={0,1,3,4})
excludeVertex :: Ord v => v -> Configuration v -> Configuration v
excludeVertex v c = updateStatus v Excluded c

-------------
-- Queries --
-------------

-- | Returns the status of a vertex in the configuration
--
-- >>> status (initConfiguration $ completeGraph 4) 1
-- Free
status :: Ord v => Configuration v -> v -> Status
status Configuration {configurationMap = m} v = 
        if isJust vertex
            then fromJust vertex
        else error "Vertex not in config"
        where vertex = Map.lookup v m


-- | Returns all vertices having the given status in the configuration
--
-- >>> verticesByStatus Free $ initConfiguration $ completeGraph 4
-- [1,2,3,4]
verticesByStatus :: Ord v => Status -> Configuration v -> [v]
verticesByStatus s Configuration {configurationMap = m} = 
                [ x | x <- Map.keys m, (fromJust $ Map.lookup x m) == s]


-- | Returns a vertex that is available for either inclusion or exclusion
--
-- Note: For sake of determinism, the returned vertex is always the minimum
-- among all available vertices.
--
-- >>> let configuration = initConfiguration $ completeGraph 4
-- >>> availableVertex configuration
-- Just 1
-- >>> let configuration' = includeVertex 1 $ configuration
-- >>> availableVertex $ configuration'
-- Just 2
-- >>> availableVertex $ includeVertex 2 $ configuration'
-- Nothing
availableVertex :: Ord v => Configuration v -> Maybe v
availableVertex c
          | length listAvailable == 0 = Nothing
          | lenIncluded == 0 = Just $ head listAvailable
          | length listAvailable' == 0 = Nothing
          | otherwise = Just $ head listAvailable'
          where listAvailable = verticesByStatus Free c
                listAvailable' = [ x | x <- listAvailable, (numStatusNeighbors Included c x) == 1]
                lenIncluded = length (verticesByStatus Included c)

-- | Returns True if the configuration is extendable.
--
-- A configuration is extendable if it has a free vertex available.
--
-- >>> extendable $ initConfiguration $ completeGraph 4
-- True
extendable :: Ord v => Configuration v -> Bool
extendable c = isJust $ availableVertex c


-- | Returns the vertices forming the subtree represented by the configuration
--
-- >>> subtree $ includeVertex 2 $ includeVertex 1 $ initConfiguration $ cycleGraph 4
-- [1,2]
-- >>> subtree $ includeVertex 1 $ includeVertex 2 $ initConfiguration $ cycleGraph 4
-- [1,2]
subtree :: Ord v => Configuration v -> [v]
subtree c = verticesByStatus Included c

-- | Returns the size of the induced subtree represented by the configuration
--
-- >>> subtreeSize $ initConfiguration $ completeGraph 4
-- 0
-- >>> subtreeSize $ includeVertex 2 $ includeVertex 1 $ initConfiguration $ cycleGraph 4
-- 2
subtreeSize :: Ord v => Configuration v -> Int
subtreeSize c = length $ subtree c

-- | Indicates whether a given vertex is a leaf in the subtree represented by
-- the configuration
--
-- >>> let c = includeVertex 2 $ includeVertex 1 $ initConfiguration $ cycleGraph 4
-- >>> [isLeaf c i | i <- [1..4]]
-- [True,True,False,False]
isLeaf :: Ord v => Configuration v -> v -> Bool
isLeaf c@(Configuration m g) v =
        if status c v == Included && (numStatusNeighbors Included c v) == 1
            then True
        else False
     
-- | Returns the number of (Current status) neighbors for a given vertex.       
numStatusNeighbors :: Ord v => Status -> Configuration v -> v -> Int 
numStatusNeighbors s c@(Configuration m g) v = 
        length [ [n] | n <- (neighbors v g), status c n == s]

-- | Returns the number of leaves in the subtree represented by the
-- configuration
--
-- >>> let c = includeVertex 2 $ includeVertex 1 $ initConfiguration $ cycleGraph 4
-- >>> numLeaves c
-- 2
numLeaves :: Ord v => Configuration v -> Int
numLeaves c@(Configuration m g) = length [ [n] | n <- vertices g, isLeaf c n == True]

--------------
-- Graphviz --
--------------

-- | Returns a Graphviz string for a given node in the configuration
--
-- >>> nodeToGraphvizString (initConfiguration $ completeGraph 4) 1
-- "\"1\" [label=\"1\", fillcolor=white];"
nodeToGraphvizString :: (Ord v, Show v) => Configuration v -> v -> String
nodeToGraphvizString c u = str ++ " [label=" ++ str ++ ", fillcolor=" ++ color st ++ "];"
    where str            = (show . show) u
          st             = status c u
          color Included = "green"
          color Excluded = "red"
          color Free     = "white"

-- | Returns a Graphviz string for a given edge in the configuration
--
-- >>> edgeToGraphvizString (initConfiguration $ completeGraph 4) (1,2)
-- "\"1\" -- \"2\";"
edgeToGraphvizString :: (Ord v, Show v) => Configuration v -> (v,v) -> String
edgeToGraphvizString c (u,w) = show2 u ++ " -- " ++ show2 w ++ ";"
    where show2 = show . show

-- | Returns a Graphviz string for the given configuration
--
-- >>> let configuration = includeVertex 2 $ includeVertex 1 $ initConfiguration $ wheelGraph 5
-- >>> putStrLn $ toGraphvizString configuration
-- graph {
--   margin=0;
--   node [fixedsize=true, width=0.3, height=0.3, style=filled];
--   "0" [label="0", fillcolor=red];
--   "1" [label="1", fillcolor=green];
--   "2" [label="2", fillcolor=green];
--   "3" [label="3", fillcolor=white];
--   "4" [label="4", fillcolor=white];
--   "5" [label="5", fillcolor=white];
-- ...
-- }
toGraphvizString :: (Ord v, Show v) => Configuration v -> String
toGraphvizString c@(Configuration m g) = intercalate "\n" [headerString, verticesString, edgeString, footerString]
    where headerString = "graph {\n" ++ "  margin=0;\n  node [fixedsize=true, width=0.3, height=0.3, style=filled];";
          footerString = "}"
          verticesString = intercalate "\n" $ map (("  "++) . nodeToGraphvizString c) (vertices g)
          edgeString = intercalate "\n" $ map (("  "++) . edgeToGraphvizString c) (edges g)
