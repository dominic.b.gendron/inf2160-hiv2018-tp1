{-|
Module      : Graph
Description : A module for playing with simple graphs
Copyright   : (c) Alexandre Blondin Massé
License     : GPL-3
Maintainer  : blondin_masse.alexandre@uqam.ca
Stability   : experimental

This module provides functionalities for playing with simple graphs. The data
structure behing @Graph@ is a strict map, associating each vertex with its
neighbors. Notice that the implementation is not optimal: Its main objective is
to explore the basic operations on lists and maps in Haskell.
 -}
module Graph (
    -- * Type and basic constructor
    Graph, emptyGraph,
    -- * Constructor of famous graph families
    completeGraph, cycleGraph, wheelGraph, completeBipartiteGraph,
    petersenGraph,
    -- * Basic queries
    -- | The usual queries can be performed on a graph.
    numVertices, numEdges, hasVertex, vertices, neighbors, hasEdge, edges,
    -- * Basic insertions
    -- | Currently, only insertions are allowed (i.e. no deletions).
    addVertex, addEdge, addEdges,
    -- * Output
    -- | One might output the graph to a GraphViz string according to the @dot@
    -- format.
    toGraphvizString,
    ) where

import qualified Data.Map.Strict as Map
import Data.List (intercalate,isPrefixOf,isSuffixOf,find)
import Data.Maybe (fromJust, fromMaybe, isJust)
import Data.Tuple (fst, snd)

-----------
-- Graph --
-----------

-- | An undirected graph.
--
-- >>> :t emptyGraph
-- emptyGraph :: Graph v
data Graph v = Graph (Map.Map v [v])

-- | Returns a string representatin of a graph
--
-- >>> show $ emptyGraph
-- "Graph of 0 vertex"
-- >>> show $ completeGraph 1
-- "Graph of 1 vertex"
-- >>> show $ completeGraph 2
-- "Graph of 2 vertices"
instance Show (Graph v) where
    show m = if numVertices m <= 1
         then "Graph of " ++ (show $ numVertices m) ++ " vertex"
         else "Graph of " ++ (show $ numVertices m) ++ " vertices"

------------------
-- Constructors --
------------------

-- | Creates an empty graph.
--
-- >>> emptyGraph
-- Graph of 0 vertex
emptyGraph :: Graph v
emptyGraph = Graph(Map.empty)

--------------------
-- Graph families --
--------------------

-- | Returns a complete graph of `n` vertices. The empty graph is returned if
-- `n <= 0`.
--
-- >>> [numVertices $ completeGraph i | i <- [1..6]]
-- [1,2,3,4,5,6]
-- >>> [numEdges $ completeGraph i | i <- [1..6]]
-- [0,1,3,6,10,15]
completeGraph :: Int -> Graph Int
completeGraph n
    | n <= 0 = emptyGraph
    | n == 1 = addVertex 1 $ emptyGraph
    | otherwise = addEdges [(x,y) | x <- [1..n], y <- [1..n], x /= y] emptyGraph


-- | Returns a cycle graph of `n` vertices. An error is triggered if `n <= 2`.
--
-- >>> [numVertices $ cycleGraph n | n <- [3..8]]
-- [3,4,5,6,7,8]
-- >>> [numEdges $ cycleGraph n | n <- [3..8]]
-- [3,4,5,6,7,8]
cycleGraph :: Int -> Graph Int
cycleGraph n 
    | n <= 2 = error "Need more than 2 verticies"
    | otherwise = cycleGraph' [1..n] n emptyGraph

-- | Subfunction of cycleGraph
cycleGraph' :: [Int] -> Int -> Graph Int -> Graph Int
cycleGraph' [] n m = emptyGraph
cycleGraph' (x:xs) n m
    | x == n = addEdge x 1 m
    | otherwise =  addEdge x (x+1) $ cycleGraph' xs n m


-- | Returns a wheel graph of `n + 1` vertices, where `n` is the number of
-- vertices surrounding the central vertex. An error is triggered if `n <= 2`.
--
-- >>> [numVertices $ wheelGraph n | n <- [3..8]]
-- [4,5,6,7,8,9]
-- >>> [numEdges $ wheelGraph n | n <- [3..8]]
-- [6,8,10,12,14,16]
wheelGraph :: Int -> Graph Int
wheelGraph n
    | n <= 2 = error "Need more than 2 verticies"
    | otherwise = addEdges middleVertex $ cycleGraph n
    where middleVertex = (zip (cycle [(0)]) [1..n])

-- | Returns a complete bipartite graph of `m + n` vertices, where `m` is the
-- number of vertices in the first part and `n` is the number of vertices in
-- the second part. The empty graph is returned if either `m < 1` or `n < 1`.
--
-- >>> [numVertices $ completeBipartiteGraph m n | m <- [1..5], n <- [1..5]]
-- [2,3,4,5,6,3,4,5,6,7,4,5,6,7,8,5,6,7,8,9,6,7,8,9,10]
-- >>> [numEdges $ completeBipartiteGraph m n | m <- [1..5], n <- [1..5]]
-- [1,2,3,4,5,2,4,6,8,10,3,6,9,12,15,4,8,12,16,20,5,10,15,20,25]
completeBipartiteGraph :: Int -> Int -> Graph Int
completeBipartiteGraph m n
    | m < 1 || n < 1 = error "Must than more than one vertex for both"
    | otherwise = completeBipartiteGraph' [1..m] [(m+1)..(m+n)] emptyGraph

-- | Subfunction of completeBipartiteGrah
completeBipartiteGraph' :: [Int] -> [Int] -> Graph Int -> Graph Int
completeBipartiteGraph' [] _ m = emptyGraph
completeBipartiteGraph' (x:xs) v2 m = addEdges listEdges $ completeBipartiteGraph' xs v2 m
        where listEdges = (zip (cycle [x]) v2)

-- | Returns the Petersen graph. See
-- https://en.wikipedia.org/wiki/Petersen_graph for more details.
--
-- >>> numVertices petersenGraph
-- 10
-- >>> numEdges petersenGraph
-- 15
petersenGraph :: Graph Int
petersenGraph = addEdges innerStar $ addEdges (zip [1..5] [6..10]) $ cycleGraph 5
    where innerStar = [(10,7),(10,8),(9,6),(9,7),(8,6)]
-------------
-- Queries --
-------------

-- | Returns the number of vertices of a graph.
--
-- >>> numVertices emptyGraph
-- 0
-- >>> numVertices $ addVertex 3 emptyGraph
-- 1
-- >>> numVertices $ addEdges [(1,2),(3,4),(1,4)] emptyGraph
-- 4
numVertices :: Graph v -> Int
numVertices (Graph m) = Map.size m


-- | Returns @True@ if and only if the given vertex is in the graph.
--
-- >>> hasVertex 2 emptyGraph
-- False
-- >>> hasVertex 3 $ addVertex 3 emptyGraph
-- True
hasVertex :: Ord v => v -> Graph v -> Bool
hasVertex v (Graph m) = Map.member v m

-- | Returns a list of all vertices in the graph.
--
-- >>> vertices emptyGraph
-- []
-- >>> let vs = vertices $ addEdges [(1,2),(3,4),(1,4)] emptyGraph
-- >>> all (`elem` vs) [1,2,3,4]
-- True
vertices :: Graph v -> [v]
vertices (Graph m) = Map.keys m

-- | Returns a list of all neighbors of some vertex in a graph.
-- If the vertex does not belong to the graph, the empty list is returned.
--
-- >>> neighbors 3 emptyGraph
-- []
-- >>> let vs = neighbors 2 $ addEdges [(1,2),(1,3),(2,3),(1,4)] emptyGraph
-- >>> all (`elem` vs) [1,3]
-- True
neighbors :: Ord v => v -> Graph v -> [v]
neighbors v (Graph m)
    | Map.lookup v m == Nothing = []
    | otherwise = fromJust $ Map.lookup v m

-- | Returns True if and only if the given vertices form an edge in the given
-- graph.
--
-- >>> hasEdge 1 4 emptyGraph
-- False
-- >>> hasEdge 3 1 $ addEdges [(1,2),(1,3)] emptyGraph
-- True
hasEdge :: Ord v => v -> v -> Graph v -> Bool
hasEdge v1 v2 (Graph m)
    | v1 == v2 = False
    | v1Found && v2Found && edgeFound = True
    | otherwise = False
    where v1Found = Map.member v1 m
          v2Found = Map.member v2 m
          edgeFound = isJust $ find (==v2) $ fromJust vertexEdges
          vertexEdges = Map.lookup v1 m



-- | Returns a list of all the edges of a graph.
--
-- >>> edges emptyGraph
-- []
-- >>> let g = addEdges [(1,2),(1,3),(3,4)] emptyGraph
-- >>> all (\(u,v) -> hasEdge u v g) (edges g)
-- True
-- >>> Data.List.sort $ edges $ addEdges [(2,1),(2,3),(3,4),(3,2)] emptyGraph
-- [(1,2),(2,3),(3,4)]
edges :: Ord v => Graph v -> [(v,v)]
edges m
    | numVertices m < 2 = []
    | otherwise = [(v,e) | v <- vertices m, e <- neighbors v m, e > v]

-- | Returns the list of the edges of vertex
edges' :: Ord v => v -> Graph v -> [(v,v)]
edges' v (Graph m) = zip (cycle[v]) m'
    where m' =  fromJust $ Map.lookup v m


-- (x:xs) v2 m = addEdges listEdges $ completeGraph' xs v2 m

-- | Returns the number of edges of a graph.
--
-- >>> numEdges emptyGraph
-- 0
-- >>> numEdges $ addVertex 3 emptyGraph
-- 0
-- >>> numEdges $ addEdges [(1,2),(3,4),(1,4)] emptyGraph
-- 3
numEdges :: Ord v => Graph v -> Int
numEdges m = length $ edges m


----------------
-- Insertions --
----------------

-- | Adds a vertex to a graph.
-- If the vertex already belongs to the graph, then the same graph is returned
-- unchanged.
--
-- >>> numVertices $ (addVertex 2 . addVertex 3) emptyGraph
-- 2
-- >>> numVertices $ (addVertex 2 . addVertex 2) emptyGraph
-- 1
addVertex :: Ord v => v -> Graph v -> Graph v
addVertex n (Graph m)
    | hasVertex n (Graph(m)) = Graph(m)
    | otherwise = Graph $ Map.insert n [] m

-- | Adds an edge to a graph.
-- If the endpoints of the edge are not vertices of the graph, they are added.
-- If the edge already belongs to the graph, then the same graph is returned
-- unchanged.
--
-- If the endpoints of the edge are the same vertex, an error is triggered.
--
-- >>> numVertices $ (addEdge 1 2) emptyGraph
-- 2
-- >>> hasEdge 2 1 $ (addEdge 1 2) emptyGraph
-- True
-- >>> addEdge 1 1 $ emptyGraph
-- ...Exception...
addEdge :: Ord v => v -> v -> Graph v -> Graph v
addEdge v1 v2 m
    | v1 == v2 = error "Both vertices are the same"
    | hasEdge v1 v2 m = m
    | v1 == v2 = m
    | otherwise = addEdge' v1 v2 $ addEdge' v2 v1 $ m'
    where m' = addVertex v1 $ addVertex v2 m



-- AddEdge detect most error cases, addEdge' do the real work
addEdge' :: Ord v => v -> v -> Graph v -> Graph v
addEdge' v1 v2 (Graph m) = Graph(Map.insert v1 ((++) [v2] $ fromJust $ Map.lookup v1 m) m)


-- | Adds a list of edges to a graph. This is equivalent to calling 'addEdge'
-- for every edge in the list.
--
-- >>> numVertices $ addEdges [(1,2),(3,4)] emptyGraph
-- 4
-- >>> hasEdge 4 3 $ addEdges [(1,2),(3,4)] emptyGraph
-- True
addEdges :: Ord v => [(v,v)] -> Graph v -> Graph v
addEdges [] m = m
addEdges (x:xs) m = addEdges xs $ addEdge v1 v2 m
    where v1 = Data.Tuple.fst x
          v2 = Data.Tuple.snd x

--------------
-- Graphviz --
--------------

-- | Returns a GraphViz string for the given graph.
--
-- >>> putStrLn $ toGraphvizString emptyGraph
-- graph {
-- <BLANKLINE>
-- <BLANKLINE>
-- }
toGraphvizString :: (Ord v, Show v) => Graph v -> String
toGraphvizString g =
    "graph {\n" ++
    (intercalate "\n" $ map graphvizVertex $ vertices g) ++ "\n" ++
    (intercalate "\n" $ map graphvizEdge $ edges g) ++ "\n" ++
    "}"
    where graphvizVertex :: Show v => v -> String
          graphvizVertex u = "  " ++ show u ++ ";"
          graphvizEdge :: Show v => (v,v) -> String
          graphvizEdge (u,v) = "  " ++ show u ++ " -- " ++ show v ++ ";"

